#!/bin/bash
# 启动服务器  ./terra_script.sh start
# 重置地图并重启服务器 ./terra_script.sh resWorlds
# 重启服务器（地图不重置） ./terra_script.sh restart
# 保存地图数据 ./terra_script.sh save
# 停止服务器 ./terra_script.sh stop
# 备份服务器根目录 ./terra_script.sh backzip
# 清理备份目录下所有文件 ./terra_script.sh clean_back
# 原作者：https://github.com/CasterWx/terraria-docker-server
# 修改者：aspnmy（support@e2bank.cn）
# 版本更新：20240618
# podman版本：https://gitcode.com/aspnmy/terraria-docker-podman-pc-server.git

# 需要非root启用，需要设置下方USERNAME为非根用户，然后对服务程序所在的path（/$HOME/opt/terraria/）设置为用户所有权即可生效
# 非根用户推荐放在（/$HOME/opt/terraria/）目录下
# 如需更方便的使用脚本命令，可以把/opt/terraria/设置到系统path中或者把/opt/terraria/terra_script.sh拷贝到/usr/bin下
# cp /$USERNAME/opt/terraria/terra_script.sh /usr/bin/terra_script
# 拷贝完成后直接可以用 sh terra_script start的形式来运行脚本，无需cd到脚本目录所在
USERNAME="root"
SCNAME="terraria"
TERA_VER="1449"
BASE_PATH="/opt/terraria/bin/$TERA_VER"
BIN_PATH="/opt/terraria/bin/$TERA_VER/Linux"
SERVICE="TerrariaServer.bin.x86_64"
#配置文件目录
FILE_DIR="/opt/terraria/config"
CONFIG="$FILE_DIR/serverconfig.txt"
RESCONFIG="$FILE_DIR/serverconfig-resWorlds.txt"
BACK_DIR="/opt/terraria/backup"
# mobile or pc
PAN_VER="pc" 

ME=`whoami`
 
if [ $ME != $USERNAME ] ; then
   echo "Please run the $USERNAME user."
   exit
fi
 
start() {
   if pgrep -u $USERNAME -f $SERVICE > /dev/null ; then
       echo "$SERVICE is already running!"
       exit
   fi
 
   echo "Starting $SERVICE..."
   screen -AmdS $SCNAME $BIN_PATH/$SERVICE -config $CONFIG
   tail -f /dev/null
   sleep 30
   exit
}
 
stop() {
   if pgrep -u $USERNAME -f $SERVICE > /dev/null ; then
       echo "Stopping $SERVICE "
   else
       echo "$SERVICE is not running!"
       exit
   fi
 
   screen -p 0 -S $SCNAME -X quit
   sleep 30
   screen -p 0 -S $SCNAME -X quit
   exit
}
 
save() {
   echo 'World data saving...'
   screen -p 0 -S $SCNAME -X eval 'stuff "say World saveing..."\015'
   screen -p 0 -S $SCNAME -X eval 'stuff "save"\015'
   exit
}
 
status() {
   if pgrep -u $USERNAME -f $SERVICE > /dev/null ; then
                echo "$SERVICE is already running!"
                exit
   else
       echo "$SERVICE is not running!"
       exit
        fi
}

restart() {
   if pgrep -u $USERNAME -f $SERVICE > /dev/null ; then
       echo "Restarting $SERVICE "
   else
       echo "$SERVICE is not running!"
       exit
   fi
   #01 save    
   screen -p 0 -S $SCNAME -X eval 'stuff "say World saveing..."\015'
   screen -p 0 -S $SCNAME -X eval 'stuff "save"\015'
   echo 'World data saving...IN 30 SECONDS.'
   sleep 30
   #02 stop
   screen -p 0 -S $SCNAME -X quit
   sleep 30
   screen -p 0 -S $SCNAME -X quit
   echo "Stopping $SERVICE ...IN 30 SECONDS."

   #03 start
   echo "Starting $SERVICE..."
   screen -AmdS $SCNAME $BIN_PATH/$SERVICE -config $CONFIG
   tail -f /dev/null
   sleep 30   
   exit
}
 
resWorlds() {
    #重置地图前需要先运行save再重置，save的备份地图在地图目录下后缀为bak
    #地图重置以后需要要生产命令重新运行，配置表中需要更改新地图的名称（默认新地图名Worlds_new.wld）
   if pgrep -u $USERNAME -f $SERVICE > /dev/null ; then
       echo "Restarting Worlds of $SERVICE "
   else
       echo "$SERVICE is not running!"
       exit
   fi
   #01 save    
   screen -p 0 -S $SCNAME -X eval 'stuff "say World saveing..."\015'
   screen -p 0 -S $SCNAME -X eval 'stuff "save"\015'
   screen -p 0 -S $SCNAME -X quit
   echo 'World data saving...IN 30 SECONDS.'
   sleep 30
   #02 resWorlds
   echo "Restarting Worlds of $SERVICE...IN 30 SECONDS."
   screen -AmdS $SCNAME $BIN_PATH/$SERVICE -config $RESCONFIG
   tail -f /dev/null
   sleep 30  
   exit
} 


#所有数据完整打包，不分系统，保持各系统版本数据一致
backzip() {
    #备份服务器主目录到指定文件夹下，可以配置定时打包备份，也可以手工打包，定时打包需要crontab组件配合
    #打包前最好先关闭服务器，如果需要用快照形式打包需要相关依赖组件，本命令非快照模式
    #01 save
    #02 stop
    #03 zip
    #打包完成以后不会自动启动服务器，需要手工用命令启动
   if pgrep -u $USERNAME -f $SERVICE > /dev/null ; then
	echo "World data backziping..."
   else
        echo "$SERVICE is not running"       
   fi

   echo 'World data backziping...IN 60 - 120 SECONDS.Do... not..closed.'
   #01 save    
   screen -p 0 -S $SCNAME -X eval 'stuff "say World saveing..."\015'
   screen -p 0 -S $SCNAME -X eval 'stuff "save"\015'
   screen -p 0 -S $SCNAME -X quit
   echo 'World data saving...IN 30 SECONDS.'
   sleep 10
   #02 stop
   screen -p 0 -S $SCNAME -X quit
   sleep 10
   screen -p 0 -S $SCNAME -X quit
   
   #03 zip 打包的时候排除backup目录下文件，这样就不需要先清理在打包
   #03 容器内存较小的时候可以先用clean_back命令清理一下backup目录
   #03 确保backup目录已经和宿主机共享的情况下，用同步脚本，或者写定时脚本备份到其他存档目录中备份  

      
   zip -r -l -o -q $BACK_DIR/$SCNAME-$PAN_VER-$TERA_VER.zip $BASE_PATH -x  $BACK_DIR*
   echo "$SERVICE data backziping...IN 30 SECONDS."
   sleep 30
   echo "$SERVICE data backzip Succeed."
   exit
} 

#清理backup目录下所有数据
clean_back() {  
   #screen -AmdS clean_back rm -rf  /root/opt/terraria/backup/*
   rm -rf  $BACK_DIR*
   echo "$SERVICE  backzips is deleting...IN 30 SECONDS."
   sleep 30
   exit
} 

case "$1" in
   start)
       start
       ;;
   stop)
       stop
       ;;
   save)
       save
       ;;
   restart)
       restart
       ;;       
   status)
       status
       ;;
    resWorlds)
       resWorlds
       ;; 
    clean_back)
       clean_back
       ;;  
    backzip)
       backzip
       ;;        
   *)
       echo  $"Usage: $0 {start|stop|status|save|restart(重启服务器但地图不重启)|resWorlds(重置地图)|backzip(zip打包所有数据)|clean_back(清理打包目录中的所有文件)}"
     
esac
