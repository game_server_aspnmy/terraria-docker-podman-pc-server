
#地图路径及名字 重置地图用的默认名字Worlds_new.wld
world=/opt/terraria/Worlds/Worlds_new.wld

maxplayers=100
#服务器访问密码留空为不需要，建议设置
password=aa123456789
worldpath=/opt/terraria/Worlds
language=zh-Hans
port = 7777
#地图种子，目前为自动模式，已知种子直接输入种子ID
seed=AwesomeSeed
motd=Welcome BlueStartOnline
banlist=banlist.txt
upnp=1
npcstream=60
priority=1

#首次运行，请以这个文件运行生成地图文件。（-config serverconfig-resWorlds）
#生成成功后，如果需要使用固定地图玩游戏，直接注释下面三个参数，或者使用-config serverconfig.txt 来启动服务器
#serverconfig.txt默认用于保存生产端服务器参数
autocreate=3
worldname=BlueStartOnline
difficulty=0




