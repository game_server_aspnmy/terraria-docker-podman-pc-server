@[TOC]
# 说明：使用buildah 打包的 游戏服务器镜像，基于前人的文章，重新基于centos stearm 9 进行打包，官方1449版本。
- 更新时间：20240622 Msg：优化了容器构建，缩小了容器体积，从850Mb缩小到了530MB，优化了性能。修复了部分sh权限bug
![在这里插入图片描述](https://img-blog.csdnimg.cn/direct/07c5a944435246d891da2900c15d3549.jpeg#pic_center)

# terraria-podman-docker-server
#### version：20240618
## 镜像托管地址：
### 1.  podman/docker/k8s：

备用私库，这个是私库访问需要登录，如果账号密码失败，需要发私信询问。

新增mini库（530MB）

```bash
podman login -u podGame -p 785@8= hub.nas.t2be.cn
podman pull hub.nas.t2be.cn/game_server/pc/terraria/1449/terraria-podman-mini:latest
```
原生库（850Mb）

```bash
podman login -u podGame -p 785@8= hub.nas.t2be.cn
podman pull hub.nas.t2be.cn/game_server/pc/terraria/1449/centos-stream-9-cn:latest
```

下载最新时间的镜像即可。


### 2.  Git地址
```
git clone https://gitcode.com/game_server_aspnmy/terraria-docker-podman-pc-server.git
```

# terraria-podman-docker-server 说明

## 前世今生
基于原作者terraria-docker-server的程序进行改造，主要适用于podman容器，构建平台是 centos steam 9 2024年6月最新版。
本版本，将原作者的docker-1353版本作为一个分支放在docker目录下面，原作者gi：https://github.com/CasterWx/terraria-docker-server

## Terraria服务器管理工具 ./terra_script.sh

 1. #启动服务器  ./terra_script.sh start
 2. #重置地图并重启服务器 ./terra_script.sh resWorlds
 3. #重启服务器（地图不重置） ./terra_script.sh restart
 4. #保存地图数据 ./terra_script.sh save
 5. #停止服务器 ./terra_script.sh stop
 6. #备份服务器根目录 ./terra_script.sh backzip
 7. #清理备份目录下所有文件 ./terra_script.sh clean_back

#### 启动服务器  ./terra_script.sh start

 - 使用screen会话形式启动服务器的，所以使用该命令启动后，如果操作窗口自己不自动退出，可以使用Ctrl+z退出即可，不影响使用。

#### 重置地图并重启服务器 ./terra_script.sh resWorlds

 - 重置地图前需要先运行save再重置，save的备份地图在地图目录下后缀为bak，地图重置以后需要要生产命令重新运行，配置表中需要更改新地图的名称（默认新地图名Worlds_new.wld）

#### 重启服务器（地图不重置） ./terra_script.sh restart
#### 保存地图数据 ./terra_script.sh save

 - /$HOME/opt/terraria/Worlds（地图备份目录）

#### 停止服务器 ./terra_script.sh stop
#### 备份服务器根目录 ./terra_script.sh backzip

 - $HOME/opt/terraria/backup(服务器备份目录)
 - #01所有数据完整打包，不分Linux/Mac/Windows系统，保持各系统版本数据一致
   备份服务器主目录到指定文件夹下，可以配置定时打包备份，也可以手工打包，定时打包需要crontab组件配合
    
 - #02打包前最好先关闭服务器，如果需要用快照形式打包需要相关依赖组件，本命令非快照模式。
    （打包完成以后**不会自动启动服务器**，需要手工用命令启动）
 -    #03 zip 打包的时候排除backup目录下文件，这样就不需要先清理在打包
 -  #03 容器内存较小的时候可以先用clean_back命令清理一下backup目录
 -  #03 确保backup目录已经和宿主机共享的情况下，用同步脚本，或者写定时脚本备份到其他存档目录中备份 
 - #04优化（缩小体积）容器建议，git中的构建包是不分系统版本的，但是实际生产环境使用的时候，可以分平台，比如Linux系统可以删除Mac和Windows两个目录，反之亦然，这样容器实际大小会变小，效能就会越高。

#### 清理备份目录下所有文件 ./terra_script.sh clean_back

 - #01清理$HOME/opt/terraria/backup(服务器备份目录)目录下所有数据
 - #02推荐使用定时脚本自动备份容器目录下$HOME/opt/terraria/backup(服务器备份目录)的备份数据到宿主机存档目录下。
 - #03可以使用定时工具配合此命令，定时清理备份数据，如需按时间筛选备份数据进行清理，请查询game_server_script.sh的独立项目对于版本（）

### 特别说明：
    原作者：https://github.com/CasterWx/terraria-docker-server
    修改者：aspnmy（support@e2bank.cn）
    版本更新：20240618
    podman版本：https://gitcode.com/game_server_aspnmy/terraria-docker-podman-pc-server.git

#### 需要非root启用，需要设置下方USERNAME为非根用户，然后对服务程序所在的path（/$HOME/opt/terraria/）设置为用户所有权即可生效
    非根用户推荐放在（/$HOME/opt/terraria/）目录下

### 全局使用 ./terra_script.sh  
    如需更方便的使用脚本命令，可以把/opt/terraria/设置到系统path中或者把/opt/terraria/terra_script.sh拷贝到/usr/bin下
    cp /$HOME/opt/terraria/terra_script.sh /usr/bin/terra_script
    拷贝完成后直接可以用 sh terra_script start的形式来运行脚本，无需cd到脚本目录所在

    PS.1449版本私有库中的镜像已经包含了所有方便操作工具及方法，只需要操作使用即可

### 1449镜像的说明

    1、podman bulid 构建文件时，dockerfile中的默认的镜像库属于私库，登录用户名密码会定期修改，如果不能访问可以发邮件询问，或者使用公共镜像中的代替镜像，推荐使用：
    cr.zsm.io/docker.io/dokken/centos-stream-9:latest


## 使用步骤

### 构建方式
1. 安装配置podman或Docker环境
2. pull基础镜像到本地(私有库如果访问不了，可以用cr.zsm.io/docker.io/dokken/centos-stream-9:latest作为基础镜像，私有库的基础镜像默认是关闭防火墙和zlinux的，并且已经安装好了依赖组件screen和zip不需要配置，如果使用公有库的镜像，还需要配置防火墙及安装screen和zip)
   
     ```bash
     buildah pull hub.nas.t2be.cn/aspnmy/baseos/centos-stream-9-cn:20240623
     ```

    或者使用（公有库）

     ```bash
     buildah pull cr.zsm.io/docker.io/dokken/centos-stream-9:latest
     ```

3. 下载git仓库中文件

```
git clone https://gitcode.com/game_server_aspnmy/terraria-docker-podman-pc-server.git
```

4. 在仓库目录terraria-docker-podman-pc-server对应版本下执行build命令
（备注：推荐使用buildah bud 来构建，构建容器体积比 使用 podman build命令要少很多。
buildah 的构建脚本查看 Containerfile
- 自己构建镜像的时候，推荐使用单系统构建，比如构建linux系统的业务，可以把/bin/{"mac","windows"}下的数据删除，再构建，本次更新的 terraria-podman-mini:latest 为了业务统一没有删除其他两个系统版本

```bash
cd   terraria-docker-podman-pc-server/terraria_server/pc_server/podman/1449
podman build -t terraria-podman-docker-server:lastest 
```

等待构建完成.

5. 基于基础镜像再优化
   - 5.1 使用podman进行优化
   - 5.1.1 下载我们共享的离线镜像包，tar导入到容器管理器podman（或docker），然后运行容器，
  ```
    podman run -p 7777:7777 -it imageid /bin/bash
  
  ```

   - 5.1.2 进入容器后，按下面的命令选择性的删除多余的系统组件(按需删除，留自己需要的组件，别都删完了)：
  ```
  # 删除mac服务器组件
  rm -rf /opt/terraria/bin/1449/Mac/*
  # 删除Windows服务器组件
  rm -rf /opt/terraria/bin/1449/Windows/*
  # 删除Linux服务器组件
  rm -rf /opt/terraria/bin/1449/Linux/*
  # 清理孤立组件并清除缓存
  dnf autoremove && dnf clean all -y 
  ```
   - 5.1.3 退出容器后进行提交，为了进一步减少容器体积，提交时把容器stop以后再提交。
  ```
  podman commit  <源镜像ID> <新镜像标签>

  ```

     - 5.2 使用buildah进行优化
   - 5.2.1 下载我们共享的离线镜像包，tar导入到容器管理器podman（或docker），然后运行容器，
  ```
    buildah from <源镜像标签>
  
  ```

   - 5.2.2 buildah run <容器名> <指令> 删除需要删除的组件：
  ```
  # 删除mac服务器组件
  buildah run <容器名> rm -rf /opt/terraria/bin/1449/Mac/*
  # 删除Windows服务器组件
  buildah run <容器名> rm -rf /opt/terraria/bin/1449/Windows/*
  # 删除Linux服务器组件
  buildah run <容器名> rm -rf /opt/terraria/bin/1449/Linux/*
  # 清理孤立组件并清除缓存
  buildah run <容器名> dnf autoremove && dnf clean all -y 
  ```
  依次执行即可。
  也可以向podman一样进入容器内部执行。
  ```
  buildah run <容器名> /bin/bash
  ```

   - 5.2.3 stop容器后进行提交，为了进一步减少容器体积。
  ```
  buildah commit  <源镜像ID> <新镜像标签>

  ```


# terraria-server-pc-1449维护说明

1. 1449的私库镜像已经配置了自动重启、每天凌晨5点自动备份地图、5的1分自动重启服务器，pull以后即可使用。

2. 推荐把

- /opt/terraria/backup(服务器备份目录)
- /opt/terraria/Worlds（地图备份目录）
- /opt/terraria/config（设置文件目录）

三个文档和宿主机共享，方便备份修改备份文件、配置文件、地图文件

# terraria-podman-docker-server 离线镜像

 - 如果需要使用1449版本对应的离线整合镜像，而不自己构建，请查看独立项目：images_server_game(https://gitcode.com/game_server_aspnmy/images_server_game.git),找到对应游戏分支下的版本镜像


# 相关资源：
1. 游戏服务器管理工具game_server_script.sh
https://gitcode.com/game_server_aspnmy/game_server_script.sh.git
2. 游戏的server服务器镜像，主要用podman运行，使用buildah构造，兼容docker和k8s，私有库需要账号密码，需要pull需要发邮件或者加入组织成员一起维护。或者按照相关教程使用公共基础镜像重新构建
https://gitcode.com/game_server_aspnmy